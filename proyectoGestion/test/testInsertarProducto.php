<?php

include (dirname(__DIR__)."../Producto.php");

class testInsertarProducto extends PHPUnit_Framework_TestCase {
	protected $producto;

    protected function setUp() {
        $this->producto = new Producto();
    }
    // Pruebas para verificar que no permita espacios vacios
    public function testInsertarNombreNulo() {
        $this->assertSame($this->producto->insertarProducto(NULL,"Producto","Producto",0,0,11,500), 0);
    }

    public function testInsertarDescripcionNula() {
        $this->assertSame($this->producto->insertarProducto("Producto",NULL,"Producto",0,0,11,500), 0);
    }
    // boolval() es para parsear int a bool porque las funciones retornan 0/1
    public function testInsertarCantidadNula() {
        $this->assertFalse(boolval($this->producto->insertarProducto("Producto","Producto","Producto",0,0,NULL,500)));
    }

    public function testInsertarPrecioNulo() {
        $this->assertFalse(boolval($this->producto->insertarProducto("Producto","Producto","Producto",0,0,11,NULL)));
    }
    /*
    // posibles pruebas -> se deben hacer validaciones antes de enviar a bd
	public function testInsertarPrecioDatoErroneo() {
        $this->assertFalse(boolval($this->producto->insertarProducto("UT","UT","UT",0,0,11,'cien')));
    }

	public function testInsertarCantidadDatoErroneo() {
        $this->assertFalse(boolval($this->producto->insertarProducto("UT","UT","UT",0,0, 'siete',500)));
    }
	*/
	public function testInsertar() {
        $this->assertTrue(boolval($this->producto->insertarProducto("Producto","Producto","Producto",1,1,15,2500)));
    }
}