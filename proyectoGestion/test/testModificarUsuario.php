<?php

include (dirname(__DIR__)."../Usuario.php");

class testModificarUsuario extends PHPUnit_Framework_TestCase {
	protected $usuario;

    protected function setUp() {
        $this->usuario = new Usuario();
    }
    /*
    //-> posibles pruebas -> se deben hacer validaciones antes de enviar a bd
    public function testModificarNombreNulo() {
        $this->assertFalse($this->usuario->modificarUsuario(111111113,NULL,'123',1));
    }

    public function testModificarClaveNula() {
        $this->assertFalse($this->usuario->modificarUsuario(111111113,"Usuario", NULL,1));
    }
*/
    public function testModificar() {
        $this->assertTrue($this->usuario->modificarUsuario(111111113,"Usuario", "123",0));
    }
}