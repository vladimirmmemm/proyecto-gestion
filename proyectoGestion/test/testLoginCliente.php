<?php

include (dirname(__DIR__)."../Cliente.php");

class testLoginCliente extends PHPUnit_Framework_TestCase {
	protected $cliente;

    protected function setUp() {
        $this->cliente = new Cliente();
    }

	public function testObtenerClientes() {
        $this->assertNotNull($this->cliente->obtenerClientes());
    }

	public function testClienteCorreoIncorrecto() {
        $this->assertSame($this->cliente->obtenerClienteLogin("test@gmail.com", "jdbdsk"), '[{"Correo_C":"0","Nombre_C":"0"}]');
    }

    public function testClienteCorreoVacio() {
        $this->assertSame($this->cliente->obtenerClienteLogin("", "jdbdsk"), '[{"Correo_C":"0","Nombre_C":"0"}]');
    }

    public function testClienteClaveIncorrecta() {
        $this->assertSame($this->cliente->obtenerClienteLogin("cris@gmail.com", "testing"), '[{"Correo_C":"0","Nombre_C":"0"}]');
    }

    public function testClienteClaveVacia() {
        $this->assertSame($this->cliente->obtenerClienteLogin("cris@gmail.com", ""), '[{"Correo_C":"0","Nombre_C":"0"}]');
    }

  	public function testDatosNoRegistrados() {
        $this->assertSame($this->cliente->obtenerClienteLogin("test@gmail.com", "testing"), '[{"Correo_C":"0","Nombre_C":"0"}]');
    }

    public function testIniciarSesion() {
        $this->assertNotNull($this->cliente->obtenerClienteLogin("cris@gmail.com", "jdbdsk"));
    }
}    
