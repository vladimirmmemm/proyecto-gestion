<?php

include (dirname(__DIR__)."../Usuario.php");

class testLoginUsuario extends PHPUnit_Framework_TestCase {
	protected $usuario;

    protected function setUp() {
        $this->usuario = new Usuario();
    }

	public function testObtenerUsuarios() {
        $this->assertNotNull($this->usuario->obtenerUsuarios());
    }

	public function testUsuarioCedulaIncorrecta() {
        $this->assertSame($this->usuario->obtenerUsuarioLogin("702550607", "123"), '[{"Cedula_U":"0","Nombre_U":"0"}]');
    }

    public function testUsuarioCedulaVacia() {
        $this->assertSame($this->usuario->obtenerUsuarioLogin("", "123"), '[{"Cedula_U":"0","Nombre_U":"0"}]');
    }

    public function testUsuarioClaveIncorrecta() {
        $this->assertSame($this->usuario->obtenerUsuarioLogin("111111113", "testing"), '[{"Cedula_U":"0","Nombre_U":"0"}]');
    }

    public function testUsuarioClaveVacia() {
        $this->assertSame($this->usuario->obtenerUsuarioLogin("111111113", ""), '[{"Cedula_U":"0","Nombre_U":"0"}]');
    }

  	public function testDatosNoRegistrados() {
        $this->assertSame($this->usuario->obtenerUsuarioLogin("702550607", "testing"), '[{"Cedula_U":"0","Nombre_U":"0"}]');
    }

    public function testIniciarSesion() {
        $this->assertNotNull($this->usuario->obtenerUsuarioLogin("111111113", "123"));
    }
}    
