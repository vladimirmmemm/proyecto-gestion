<?php

include (dirname(__DIR__)."../Producto.php");

class testEliminarProducto extends PHPUnit_Framework_TestCase {
	protected $producto;
	protected $codigo;

    protected function setUp() {
        $this->producto = new Producto();
        $this->codigo = $this->producto->obtenerUltimoInsertado().",";
    }

    public function testEliminar() {
        $this->assertTrue(boolval($this->producto->eliminarProducto($this->codigo)));
    }
}