<?php

include (dirname(__DIR__)."../Cliente.php");

class testCliente extends PHPUnit_Framework_TestCase {
	protected $cliente;

    protected function setUp() {
        $this->cliente = new Cliente();
    }

    // Pruebas para insertar Cliente
    public function testInsertarCorreoNulo() {
        $this->assertFalse($this->cliente->registrarClientes(NULL,"Cliente","99999999","123"));
    }

    public function testInsertarNombreNulo() {
        $this->assertFalse($this->cliente->registrarClientes("Cliente@gmail.com",NULL,"99999999","123"));
    }

    public function testInsertarTelefonoNulo() {
        $this->assertFalse(boolval($this->cliente->registrarClientes("Cliente@gmail.com","Cliente",NULL,"123")));
    }

    public function testInsertarClaveNula() {
        $this->assertFalse(boolval($this->cliente->registrarClientes("Cliente@gmail.com","Cliente","99999999",NULL)));
    }
    /*
    // posibles pruebas -> se deben hacer validaciones antes de enviar a bd
	*/
	public function testInsertar() {
        $this->assertTrue(boolval($this->cliente->registrarClientes("Cliente@gmail.com","Cliente","99999999","123")));
    }

    // Pruebas para consultar Clientes
    public function testConsultarClientes() {
    	$this->assertNotNull($this->cliente->obtenerClientes());
    }

    // Pruebas para modificar Clientes
	public function testModificar() {
        $this->assertTrue(boolval($this->cliente->modificarCliente("Cliente@gmail.com","Cliente","22222222","123")));
    }

    // Pruebas para modificar Clientes
    public function testEliminar() {
        $this->assertTrue(boolval($this->cliente->eliminarCliente("Cliente@gmail.com")));
    }
}
?>