<nav>
    <div class="nav-fostrap">
        <ul>

            <li><a href="../Empleado/inicioH.php"><i class='fas fa-home'></i> Inicio</a></li>
            <li><a href="../Empleado/productoH.php"><i class='fas fa-project-diagram'></i> Productos</a></li>
            <li><a href="../Empleado/proveedorH.php"><i class='fab fa-product-hunt'></i> Proveedores</a></li>
            <li><a href="../Empleado/categoriaH.php"><i class='fas fa-certificate'></i> Categorias</a></li>
            <li><a href="../Empleado/clienteH.php"><i class='fas fa-users'></i> Cliente</a></li>

            <li class="nav-right">
                <a onclick="cerrarSesion()" style="cursor: pointer"><i class='fas fa-door-open'></i>Cerrar Sesión</a>
            </li>

        </ul>
    </div>
</nav>
<div id="nav-back"></div>

<script>
    function cerrarSesion() {
        $.post("../Sesion.php",{ sesion: "cerrar"})
            .done(function (ippreg){
                location.href = "../index.php";
            });
    }
</script>
