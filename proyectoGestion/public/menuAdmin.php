<nav>
    <div class="nav-fostrap">
        <ul>
            <li class="title-web">
                <a href="/">ASADAS</a>
            </li>
            <li><a href="javascript:void(0)">Administración<span class="arrow-down ml-2"></span></a>
                <ul class="dropdown">
                    <li><a href="/noticia">Noticias</a></li>
                    <li><a href="/colaborador">Colaboradores</a></li>
                    <li><a href="/donacion">Donaciones</a></li>
                </ul>
            </li>
            <li><a href="javascript:void(0)">Navegación usuario<span class="arrow-down ml-2"></span></a>
                <ul class="dropdown">
                    <li><a href="/">Noticias</a></li>
                    <li><a href="/colaboradores">Colaboradores</a></li>
                    <li><a href="/donaciones">Donaciones</a></li>
                </ul>
            </li>
            <li class="nav-right">
                <a href="{{ url('/logout') }}">Cerrar Sesión</a>
            </li>
        </ul>
    </div>
    <div class="nav-bg-fostrap">
        <div class="navbar-fostrap"> <span></span> <span></span> <span></span> </div>
        <a href="/" class="title-mobile">ASADAS</a>
    </div>
</nav>
<div id="nav-back"></div>
