<nav>
    <div class="nav-fostrap">
        <ul>
            <li class="title-web">
                <a href="">Distribuidora Gónzalez</a>
            </li>

            <li><a href="../Cliente/inicioH.php"><i class='fas fa-home'></i> Inicio</a></li>
            <li><a href="../Cliente/noticiasH.php"><i class='far fa-newspaper'></i> Noticias</a></li>
            <li><a href="../Cliente/informacionH.php"><i class='fas fa-info-circle'></i> Información</a></li>
            <li><a href="../Cliente/contactosH.php"><i class='fas fa-clipboard-list'></i> Contactos</a></li>
            <li><a href="../Cliente/catalogoH.php"><i class='fas fa-shopping-basket'></i> Catálogo</a></li>

            <ul class="nav-right">
                <li>
                    <a href="../login/loginCliente.php"><i class='far fa-user-circle'></i> Iniciar Sesión</a>
                </li>
                <li>
                    <a href=""><i class='fas fa-door-open'></i> Cerrar Sesión</a>
                </li>
            </ul>

        </ul>
    </div>
</nav>
<div id="nav-back"></div>
