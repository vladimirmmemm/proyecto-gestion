<!DOCTYPE html lang="es">
<html lang="es">
    <?php
      include("../public/head.php");
    ?>

    <script src="../script/general.js"></script>
    <script src="../script/productoH.js"></script>

    <title>Producto</title>
  </head>
  <body>
    <?php
      include("../public/menuEmpleado.php");
    ?>
    <div class="container mt-5 mb-5">
        <h3 class="color-1">Productos</h3>
        <hr></hr>
        <div id="listadoProductos">
            <div class="mb-3">
                <button id="eliminar" type="button" data-tooltip="tooltip" data-placement="top" title="Eliminar selección" class="cont-icono btn btn-outline-danger" disabled><i class="far fa-trash-alt"></i></button>
                <a id="agregar" class="cont-icono btn btn-outline-primary float-right" data-tooltip="tooltip" data-placement="top" title="Agregar producto" onclick="abrirModalAgregar()"><i class="far fa-plus-square"></i></a>
            </div>
            <div class="mb-5">
                <table id="tbProductos" class="table table-striped table-bordered dt-responsive display">
                    <thead>
                        <tr>
                            <th class="multi-check-cont">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="multi-check"/>
                                    <label class="custom-control-label ml-2" htmlFor="multi-checkbox"></label>
                                </div>
                            </th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Categoría</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Proveedor</th>
                            <th>Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalGeneralProducto" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-sm-11">
                            <h3 class="modal-title" id="tituloModal"></h3>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body" id="contenidoModal">
                        <div class="form-group">
                            <label for="nombreProducto">Nombre Producto: </label>
                            <input type="text" class="form-control" id="Nombre_P" onkeypress="return soloLetras(event);">
                        </div>
                        <div class="form-group"> 
                            <label for="descripcionProducto">Descripción Producto:</label>
                            <input type="text" class="form-control" id="Descripcion_P" onkeypress="return soloLetras(event);">
                        </div>
                        <div class="form-group"> 
                            <label for="categoriaProducto">Categoría Producto:</label>
                            <select class="custom-select" id="Categoria_P">
                            </select>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label for="precio">Precio:</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">‎₡</span>
                                    </div>
                                <input type="text" class="form-control" id="Precio_P" onkeypress="return somenteNumeroDecimal(this,event);" maxlength="7" required>
                                </div>
                            </div>
                            <div class="form-group col-lg-6"> 
                                <label for="cantidadProducto">Stock Producto:</label>
                                <input type="text" class="form-control" id="Cantidad_P" onKeyPress="return soloNumeros(event)" maxlength="6" required>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <label for="proveedorProducto">Proveedor Producto:</label>
                            <select class="custom-select" id="Proveedor_P">              
                            </select>
                        </div>
                        <div id="vizualizarImagen">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="btnAccion"></button>
                    </div>
                </div>
            </div>
        </div>
   </body>
</html>



