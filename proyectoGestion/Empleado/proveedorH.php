<!DOCTYPE html lang="es">
<html lang="es">
    <?php
    include("../public/head.php");
    ?>

    <script src="../script/proveedorH.js "></script>
    <script src="../script/general.js"></script>
    <title>Proveedor</title>
</head>
<body>
<?php
include("../public/menuEmpleado.php");
?>

<!-- tabla -->
<div class="container mt-5 mb-5">
    <h3 class="color-1">Proveedores</h3>
    <hr></hr>
    <div id="listadoProveedores">
        <div class="mb-3">
            <a id="registarUsuario" class="cont-icono btn btn-outline-primary float-right" data-tooltip="tooltip" data-placement="top" title="Agregar usuario" onclick="abrirModalRegistrarProveedor()"><i class="far fa-plus-square"></i></a>
        </div>
        <div class="mb-5">
            <table id="tbProveedor" class="table table-striped table-bordered dt-responsive display">

            </table>
        </div>
    </div>
</div>

<!-- modal -->
<div class="modal fade" id="modalProveedor" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-sm-11">
                    <h3 class="modal-title" id="tituloModalProveedor"></h3>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body" id="contenidoModalProveedor">
                 <div class="form-group">
                  
                    <input type="hidden" class="form-control" id="idProveedor" >
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre Completo: </label>
                    <input type="text" class="form-control" id="nombreProveedor" onkeypress="return soloLetras(event);" >
                </div>
                <div class="form-group">
                    <label for="correo">Correo electrónico:</label>
                    <input type="email" class="form-control" id="correoProveedor" value="" maxlength="255"  onkeypress="return soloCorreo(event);" maxlength="100" required>
                </div>
                <div class="form-group">
                     <label for="correo">Teléfono:</label>
                    <input type="text" class="form-control" id="TelefonoProveedor" onkeypress="return soloNumeros(event);"maxlength="8">
                </div>
                <div class="form-group">
                   
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnProveedor"></button>
            </div>
        </div>
    </div>
</div>
</body>
</html>




