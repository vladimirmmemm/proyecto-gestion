<!DOCTYPE html lang="es">
<html lang="es">
    <?php
    include("../public/head.php");
    ?>

    <script src="../script/categoriaH.js"></script>
    <script src="../script/general.js"></script>
    <title>Categoria</title>
  </head>
  <body>
    <?php
      include("../public/menuEmpleado.php");
    ?>   

    <!-- tabla -->
    <div class="container mt-5 mb-5">
        <h3 class="color-1">Categorias</h3>
        <hr></hr>
        <div id="listadoUsuarios">
            <div class="mb-3">
                <a id="registarCategoria" class="cont-icono btn btn-outline-primary float-right" data-tooltip="tooltip" data-placement="top" title="Agregar categoria" onclick="abrirModalRegistrarCategoria()"><i class="far fa-plus-square"></i></a>
            </div>
            <div class="mb-5">
               <table id="tbCat" class="table table-striped table-bordered dt-responsive display">
        
                </table>
            </div>
        </div>
    </div>
    
    <!-- modal -->
    <div class="modal fade" id="modalCategoria" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-sm-11">
                            <h3 class="modal-title" id="tituloModalCategoria"></h3>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body" id="contenidoModalCategoria">
                        <div class="form-group">
                           
                            <input type="hidden" class="form-control" id="CodigoCategoria" onkeypress="return soloLetras(event);">
                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre: </label>
                            <input type="text" class="form-control" id="nombreCategoria" onkeypress="return soloLetras(event);">
                        </div>
                        <div class="form-group"> 
                           
                        </div>                      
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                              
                            </div>
                        </div>
                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="btnCategoria"></button>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>



