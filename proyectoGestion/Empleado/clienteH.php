<!doctype html>
<html lang="es">
    <?php
    include("../public/head.php");
    ?>

    <script src="../script/general.js"></script>
    <script src="../script/clienteH.js"></script>

    <title>Producto</title>
</head>
<body>
    <?php
    include("../public/menuEmpleado.php");
    ?>
    <div class="container mt-5 mb-5">
        <h3 class="color-1">Clientes</h3>
        <hr></hr>
        <div id="listadoClientes">
            <div class="mb-3">
                <button id="eliminar" type="button" data-tooltip="tooltip" data-placement="top" title="Eliminar selección" class="cont-icono btn btn-outline-danger" disabled><i class="far fa-trash-alt"></i></button>
                <a id="agregar" class="cont-icono btn btn-outline-primary float-right" data-tooltip="tooltip" data-placement="top" title="Agregar producto" onclick="abrirModalAgregar()"><i class="far fa-plus-square"></i></a>
            </div>
            <div class="mb-5">
                <table id="tbCliente" class="table table-striped table-bordered dt-responsive display">
                    <thead>
                    <tr>
                        <th class="multi-check-cont">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="multi-check"/>
                                <label class="custom-control-label ml-2" htmlFor="multi-checkbox"></label>
                            </div>
                        </th>
                        <th>Correo</th>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                        <th>Contraseña</th>
                        <th>Editar</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalGeneralProducto" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-sm-11">
                        <h3 class="modal-title" id="tituloModal"></h3>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="modal-body" id="contenidoModal">
                    <div class="form-group">
                        <label for="nombreCliente">Correo del Cliente: </label>
                        <input type="text" class="form-control" id="Correo_C" onkeypress="return soloCorreo(event);" maxlength="100" required>
                    </div>
                    <div class="form-group">
                        <label for="Nombre_Cliente">Nombre del Cliente:</label>
                        <input type="text" class="form-control" id="Nombre_C" onkeypress="return soloLetras(event);" maxlength="45" required>
                    </div>
                    <div class="form-group">
                        <label for="TelefonoCliente">Teléfono del Cliente:</label>
                        <input type="text" class="form-control" id="telefono_C" onkeypress="return soloNumeros(event);" maxlength="8" required>
                    </div>
                    <div class="form-group">
                        <label for="passwordCliente">Contraseña del Cliente:</label>
                        <input type="text" class="form-control" id="password_C" onkeypress="return soloLetras(event);" maxlength="15" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnAccion"></button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>