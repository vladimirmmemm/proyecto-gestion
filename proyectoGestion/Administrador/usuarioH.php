<!DOCTYPE html lang="es">
<html lang="es">
  <?php
      include("../public/head.php");
  ?>

    <script src="../script/usuarioH.js"></script>
    <script src="../script/general.js"></script>
    <title>Usuario</title>

  <body>
    <?php
      include("../public/menuUsuario.php");
    ?>   

    <!-- tabla -->
    <div class="container mt-5 mb-5">
        <h3 class="color-1">Usuarios</h3>
        <hr></hr>
        <div id="listadoUsuarios">
            <div class="mb-3">
                <a id="registarUsuario" class="cont-icono btn btn-outline-primary float-right" data-tooltip="tooltip" data-placement="top" title="Agregar usuario" onclick="abrirModalRegistrarUsuario()"><i class="far fa-plus-square"></i></a>
            </div>
            <div class="mb-5">
               <table id="tbUsuarios" class="table table-striped table-bordered dt-responsive display">
        
                </table>
            </div>
        </div>
    </div>
    
    <!-- modal -->
    <div class="modal fade" id="modalUsuario" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-sm-11">
                            <h3 class="modal-title" id="tituloModalUsuario"></h3>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body" id="contenidoModalUsuario">
                        <div class="form-group">
                            <label for="cedula">Cédula: </label>
                            <input type="text" class="form-control" id="cedulaUsuario" onkeypress="return soloNumeros(event);" maxlength="9">
                        </div>
                        <div class="form-group"> 
                            <label for="nombre">Nombre Completo:</label>
                            <input type="text" class="form-control" id="nombreUsuario" onkeypress="return soloLetras(event);">
                        </div>                      
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="tipoUsuario">
                                <label class="custom-control-label" for="tipoUsuario">Administrador
                                </label>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <label for="clave">Contraseña:</label>
                            <input type="password" class="form-control" id="claveUsuario">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="btnUsuario"></button>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <?php
      include("../public/footer.php");
    ?>
</html>



