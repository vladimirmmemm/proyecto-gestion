//Variables Globales
$listaCategorias = {};
$tablaCategorias = null;

$( document ).ready(function() {
    obtenerCategorias();
});

//Funcion obtener lista usuarios
function obtenerCategorias(){

    $.ajax({
        url: '../data/Categoria.php',
        type: 'GET',
        data: {obtenerCategoria: "true"},
        success: function (resultado) {
           $listaCategorias = jQuery.parseJSON(resultado);
             inicializarTablaCategorias();
        }
    });
}

//Funcion abrir modal Registrar usuario
function abrirModalRegistrarCategoria(){
    limpiarCamposCategoria();
   
    $('#tituloModalCategoria').text('Registrar Categoría');
    $('#btnCategoria').text('Agregar');
    $('#btnCategoria').attr('onclick', 'registarCategoria()');
    $('#modalCategoria').modal('show');
}

//Funcion abrir modal modificar usuario
function abrirModalModificarCategoria($data) {
    $('#tituloModalCategoria').text('Modificar Categoría');
    $('#btnCategoria').text('Modificar');
    $('#btnCategoria').attr('onclick', 'modificarCategoria()');
    $('#modalCategoria').modal('show');

    $posicion = buscarCategoria($data);

    $('#CodigoCategoria').val($listaCategorias[$posicion].Codigo_Ca);
    $('#nombreCategoria').val($listaCategorias[$posicion].Nombre_Ca);
   
   
    document.getElementById("CodigoCategoria").readOnly = true;
}

function registarCategoria() {

    $nombreCategoria = $('#nombreCategoria').val();


    //verificar que no estén vacíos
    if ($nombreCategoria == "" ) {
        swal("Error", "Ocurrió un error al agregar la nueva categoría en el sistema, es necesario llenar TODOS los datos!", "error");
    } else {
        $.ajax({
            url: '../data/Categoria.php',
            type: 'POST',
            dataType: 'html',
            data: {insertarCategoria: "true", Nombre_Ca:$nombreCategoria},
            success: function (resultado) {
                if(resultado == 1){
                   limpiarCamposCategoria();
                    swal("Operación Exitosa", "Se ha generado una nueva categoría para " + $nombreCategoria + " en el sistema!", "success");
                   obtenerCategorias();
                    $('#modalCategoria').modal('hide');
                }else{
                    swal("Error", "Ocurrió un error al agregar la nueva categoría en el sistema!", "error");
                }
            }
        });

    }
}

function modificarCategoria() {
    $CodigoCategoria = $('#CodigoCategoria').val();
    $nombreCategoria = $('#nombreCategoria').val();
   

    //verificar que no estén vacíos
    if ($nombreCategoria == "") {
        swal("Error", "Ocurrió un error al modificar la Categoría en el sistema, es necesario llenar TODOS los datos!", "error");
    } else {
        $.ajax({
            url: '../data/Categoria.php',
            type: 'POST',
            dataType: 'html',
            data: {modificarCategoria: "true", Codigo_Ca: $CodigoCategoria, Nombre_Ca: $nombreCategoria},
            success: function (resultado) {
                if(resultado == 1){
                    limpiarCamposCategoria();
                    swal("Operación Exitosa", "Se ha modificado la información de la Categoría " +  $nombreCategoria + " en el sistema!", "success");
                  obtenerCategorias();
                    $('#modalCategoria').modal('hide');
                }else{
                    swal("Error", "Ocurrió un error al modificar la Categoría en el sistema!", "error");
                }
            }
        });

    }
}
        
function eliminarCategoria($codigo) {
    // obtener el index de la lista del usuario a eliminar
    $posicion = buscarCategoria($codigo);

    swal({
        title: "Confirmar Operación",
        text: "Al confirmar se eliminará la categoría '" + $listaCategorias[$posicion].Nombre_Ca +"' del sistema, esta operación no es reversible!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            // debe enviar a eliminar en backend {bd}
            $.ajax({
                url: '../data/Categoria.php',
                type: 'POST',
                data: {eliminarCategoria: "true", Codigo_Ca: $codigo},
                success: function (resultado) {
                    if(resultado == 1){

                        swal("Operación Exitosa", "la categoría '" + $listaCategorias[$posicion].Nombre_Ca + "'' fue eliminada correctamente del sistema!", "success");
                        obtenerCategorias();
                    }else{
                        swal("Error", "Ocurrió un fallo al eliminar la categoría " + $listaCategorias[$posicion].Nombre_Ca , "error");
                    }
                }
            });

        } else {
            swal("Operación Cancelada", "No se ha eliminado la categoría del sistema!", "success");
        }
    });
}

//Funcion obtener el objeto producto
function buscarCategoria(codigo){

    $posicionProducto = 0;

    for(var i = 0; i < $listaCategorias.length; i++){
        if( $listaCategorias[i].Codigo_Ca == codigo){
            $posicionProducto = i;
            i = $listaCategorias.length
        }
    }
    return $posicionProducto;
}

function limpiarCamposCategoria(){
    $('#nombreCategoria').val("");
  
  
}

//Se crea la tabla 
function inicializarTablaCategorias() { 
    if($tablaCategorias != null){
       $tablaCategorias.clear().destroy();
       $tablaCategorias = null
    }

    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });

    $tablaCategorias = $('#tbCat').DataTable({
        destroy: true,
        "scrollx": true,
        "lengthMenu": [ 4 ],
        data: $listaCategorias,
        columns: [
         
            { title: "Nombre", data: "Nombre_Ca"  },

         
            { title: "Editar" },
              { title: "Elim" },
        ],
        "language": {"url": "json/configDatatable.json"},
        "columnDefs": [ 
        {
            "targets": 1,
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {                                                                                                                                                                                     
                 return "<button id='modificarCategoria' type='button' data-toggle='tooltip' data-placement='top' title='Modificar selección' class='cont-icono btn btn-outline-succes'><i class='fas fa-edit' onclick='abrirModalModificarCategoria(" + data.Codigo_Ca + ")' ></i></button>";
            }
        },
        {
            "targets": 2,
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                 return "<button id='eliminarCategoria' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger'><i class='far fa-trash-alt' onclick='eliminarCategoria(" + data.Codigo_Ca + ")'></i></button>"
            }
        }
        ],
        "order": [[0, "asc"]],
        "autoWidth": false,
        "preDrawCallback": function(settings) {
                $("#tbCat tbody tr").removeClass("seleccionado");
                $("#tbCat tbody tr td").removeClass("selected");
            },
        "drawCallback": function(settings) {
            //$("#eliminarUsuario").prop("disabled", true);
            $("#tbCat tbody tr").removeClass("selected");
            $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        }
    });
}
