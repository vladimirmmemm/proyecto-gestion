//Variables Globales
$listaProveedores = {};
$tablaProveedores = null;

$( document ).ready(function() {
    obtenerProveedores();
});

//Funcion obtener lista proveedores
function obtenerProveedores(){

    $.ajax({
        url: '../data/Proveedor.php',
        type: 'GET',
        data: {obtenerProveedores: "true"},
        success: function (resultado) {
            $listaProveedores = jQuery.parseJSON(resultado);
            inicializarTablaProveedores();
        }
    });
}

//Funcion abrir modal Registrar usuario
function abrirModalRegistrarProveedor(){
    limpiarCamposProveedor();
   // document.getElementById("cedulaUsuario").readOnly = false;
    $('#tituloModalProveedor').text('Registrar Proveedor');
    $('#btnProveedor').text('Agregar');
    $('#btnProveedor').attr('onclick', 'registarProveedor()');
    $('#modalProveedor').modal('show');
}

//Funcion abrir modal modificar usuario
function abrirModalModificarProveedor($data) {
    $('#tituloModalProveedor').text('Modificar Proveedor');
    $('#btnProveedor').text('Modificar');
    $('#btnProveedor').attr('onclick', 'modificarProveedor()');
    $('#modalProveedor').modal('show');

    $posicion = buscarProveedor($data);
    
    $('#idProveedor').val($listaProveedores[$posicion].Id_P);
    $('#nombreProveedor').val($listaProveedores[$posicion].Nombre_P);
    $('#correoProveedor').val($listaProveedores[$posicion].Correo_P);
    $('#TelefonoProveedor').val($listaProveedores[$posicion].Telefono_P);
  
}

function registarProveedor() {

     var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

    if (regex.test($('#correoProveedor').val().trim())) {
        $nombreProveedor = $('#nombreProveedor').val();
    $correoProveedor = $('#correoProveedor').val();
    $TelefonoProveedor = $('#TelefonoProveedor').val();
    //bool
    

    //verificar que no estén vacíos
    if ($nombreProveedor == "" || $correoProveedor == "" || $TelefonoProveedor == "") {
        swal("Error", "Ocurrió un error al agregar el nuevo proveedor en el sistema, es necesario llenar TODOS los datos!", "error");
    } else {
        $.ajax({
            url: '../data/Proveedor.php',
            type: 'POST',
            dataType: 'html',
            data: {insertarProveedor: "true", Nombre_P: $nombreProveedor, Correo_P: $correoProveedor, Telefono_P: $TelefonoProveedor},
            success: function (resultado) {
                if(resultado == 1){
                    limpiarCamposProveedor();
                    swal("Operación Exitosa", "Se ha ingresado un nuevo proveedor " + $nombreProveedor + " en el sistema!", "success");
                    obtenerProveedores();
                    $('#modalProveedor').modal('hide');
                }else{
                    swal("Error", "Ocurrió un error al agregar el nuevo proveedor en el sistema!", "error");
                }
            }
        });

    }
    }else{
         alertify.error("La dirección de correo no es válida");
    }
   
}



function modificarProveedor() {
    $Id_Proveedor= $('#idProveedor').val();
    // swal("Operación Exitosa", "Se " + $Id_Proveedor + " hola!", "success");
    $nombreProveedor = $('#nombreProveedor').val();
    $correoProveedor = $('#correoProveedor').val();
    $TelefonoProveedor = $('#TelefonoProveedor').val();
    //bool
   

    //verificar que no estén vacíos
    if ($nombreProveedor == "" || $correoProveedor == "" || $TelefonoProveedor== "") {
        swal("Error", "Ocurrió un error al modificar el proveedor en el sistema, es necesario llenar TODOS los datos!", "error");
    } else {
        $.ajax({
            url: '../data/Proveedor.php',
            type: 'POST',
            dataType: 'html',
            data: {modificarProveedor: "true",Id_P:$Id_Proveedor, Nombre_P: $nombreProveedor, Correo_P: $correoProveedor, Telefono_P:$TelefonoProveedor},
            success: function (resultado) {
                if(resultado == 1){
                    limpiarCamposProveedor();
                    swal("Operación Exitosa", "Se ha modificado la información del proveedor " + $nombreProveedor + " en el sistema!", "success");
                    obtenerProveedores();
                    $('#modalProveedor').modal('hide');
                }else{
                    swal("Error", "Ocurrió un error al modificar el proveedor en el sistema!", "error");
                }
            }
        });

    }
}

function eliminarProveedor($id) {
    // obtener el index de la lista del usuario a eliminar
    $posicion = buscarProveedor($id);

    swal({
        title: "Confirmar Operación",
        text: "Al confirmar se eliminará al usuario '" + $listaProveedores[$posicion].Nombre_P +"' del sistema, esta operación no es reversible!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: '../data/Proveedor.php',
                type: 'POST',
                data: {eliminarProveedor: "true", Id_P: $id},
                success: function (resultado) {
                    if(resultado == 1){
                        swal("Operación Exitosa", "El proveedor'" + $listaProveedores[$posicion].Nombre_P + "'' fue eliminado correctamente del sistema!", "success");
                        obtenerProveedores();
                    }else{
                        swal("Error", "Ocurrió un fallo al eliminar el proveedor " + $listaProveedores[$posicion].Nombre_P , "error");
                    }
                }
            });
        } else {
            swal("Operación Cancelada", "No se ha eliminado al proveedor del sistema!", "success");
        }
    });
}

//Funcion obtener el objeto proveedor
function buscarProveedor(id){

    $posicionProveedor = 0;

    for(var i = 0; i < $listaProveedores.length; i++){
        if( $listaProveedores[i].Id_P == id){
            $posicionProveedor = i;
            i = $listaProveedores.length
        }
    }
    return $posicionProveedor;
}

function limpiarCamposProveedor(){
    $('#nombreProveedor').val("");
    $('#correoProveedor').val("");
    $('#TelefonoProveedor').val("");
    
}

//Se crea la tabla
function inicializarTablaProveedores() {
    if($tablaProveedores != null){
        $tablaProveedores.clear().destroy();
        $tablaProveedores = null
    }

    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });

    $tablaProveedores = $('#tbProveedor').DataTable({
        destroy: true,
        "scrollx": true,
        "lengthMenu": [ 4 ],
        data: $listaProveedores,
        columns: [
            { title: "Nombre", data: "Nombre_P" },
            { title: "Correo", data: "Correo_P"  },
            { title: "Teléfono", data: "Telefono_P"  },
            { title: "Editar" },
            { title: "Eliminar" },
        ],
        "language": {"url": "json/configDatatable.json"},
        "columnDefs": [
            {
                "targets": 3,
                "data": null,
                "orderable": false,
                "width": "5%",
                "className": "text-center bg-white",
                "mData": function (data, type, val) {
                    return "<button id='modificarProveedor' type='button' data-toggle='tooltip' data-placement='top' title='Modificar selección' class='cont-icono btn btn-outline-succes'><i class='fas fa-edit' onclick='abrirModalModificarProveedor(" + data.Id_P + ")'></i></button>";
                }
            },
            {
                "targets": 4,
                "data": null,
                "orderable": false,
                "width": "5%",
                "className": "text-center bg-white",
                "mData": function (data, type, val) {
                    return "<button id='eliminarProveedor' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger'><i class='far fa-trash-alt' onclick='eliminarProveedor(" + data.Id_P + ")'></i></button>"
                }
            }
        ],
        "order": [[0, "asc"]],
        "autoWidth": false,
        "preDrawCallback": function(settings) {
            $("#tbProveedor tbody tr").removeClass("seleccionado");
            $("#tbProveedor tbody tr td").removeClass("selected");
        },
        "drawCallback": function(settings) {
            //$("#eliminarUsuario").prop("disabled", true);
            $("#tbProveedor tbody tr").removeClass("selected");
            $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        }
    });
}
