$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
});

function login() {
    $user = $("#user").val();
    $pass = $("#password").val();

    $.ajax({
        url: '../data/Cliente.php',
        type: 'POST',
        data: {obtenerClienteLogin: "true", Correo_C: $user, Password_C: $pass},
        success: function (resultado) {
            $cliente = jQuery.parseJSON(resultado);
            // cuándo es incorrecto retorna [0,0]
            if($cliente[0].Password_C != 0){
                alertify.error('ÉXITO');
            }else{
                alertify.error('Usuario o Contraseña incorrecta');
                limpiarCampos();
            }
        }
    });
}

function limpiarCampos() {
    $("#login").val("");
    $("#password").val("");
}

function limpiarBordes() {
    document.getElementById("correo").classList.remove("is-invalid");
    document.getElementById("password1").classList.remove("is-invalid");
    document.getElementById("password2").classList.remove("is-invalid");
}

function registrarse() {
    limpiarBordes();
    var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

    if (regex.test($('#correo').val().trim())) {

        $correo = $("#correo").val();
        $nombre = $("#nombre").val();
        $tel = $("#telfono").val();
        $pass = $("#password1").val();
        $pass2 = $("#password2").val();

        if($pass === $pass2){

            if($correo != "" && $nombre != "" && $tel != "" && $pass != "") {
                $.ajax({
                    url: '../data/Cliente.php',
                    type: 'POST',
                    dataType: 'html',
                    data: {registrarClientes: "true", Correo_C: $correo, Nombre_C: $nombre, Telefono_C: $tel, Password_C: $pass},
                    success: function (resultado) {
                        console.log(resultado);
                        if(resultado == 1){
                            alertify.error('ÉXITO');
                        }else{
                            alertify.error('No se pudo realizar la transacción, inténtelo más tarde');
                        }
                    }
                });
            }else{
                alertify.error('Se encontraron campos en blanco.');
            }
        }else{
            alertify.error('Las contraseñas no son iguales');
            document.getElementById("password1").classList.add("is-invalid");
            document.getElementById("password2").classList.add("is-invalid");
        }



    } else {
        document.getElementById("correo").classList.add("is-invalid");
        alertify.error('La dirección de correo no es válida');
    }
}


function recuperarClave() {
    swal({
        title: "Recuperar Contraseña!",
        text: "Al realizar esta operación se te enviará al correo electrónico ingresado un enlace para restablecer tú contraseña:",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'Enviar',
        cancelButtonText: 'Cancelar',
        animation: "slide-from-top",
        inputPlaceholder: "Ingresa tú correo electrónico"
    },
    function(inputValue){
        if (inputValue != "") {
            $.ajax({
                url: '../data/Cliente.php',
                    type: 'POST',
                    data: {modificarClaveCliente: "true", Correo_C: inputValue},
                    success: function (resultado) {
                        if(resultado == 1){
                            swal("Operación Exitosa", "Se ha enviado al correo '"+ inputValue +"' un enlace para restablecer tú contraseña!","success");
                        }else{
                            swal("Error", "Ha ocurrido error al intentar restablecer la contraseña del cliente" , "error");
                        }
                    }
                });
        } else {
            swal({
                title: "Error",
                text: "Debes ingresar un correo electrónico!",
                type: "error",
                showCancelButton: false,
                buttonsStyling: false,
                closeOnConfirm: false
            }, function(isConfirm) {
                if (isConfirm) {
                    recuperarClave();
                } 
            });
        }
    });
}