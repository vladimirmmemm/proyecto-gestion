function login() {
    $user = $("#login").val();
    $pass = $("#password").val();

    $.ajax({
        url: '../data/Usuario.php',
        type: 'GET',
        data: {obtenerUsuarioLogin: "true", "Cedula_U": $user,"Password_U":$pass},
        success: function (resultado) {
            $usuario = jQuery.parseJSON(resultado);
            // cuándo es incorrecto retorna [0,0]
            if($usuario[0].Password_U != 0){
                alertify.error('ÉXITO');
            }else{
                alertify.error('Usuario o Contraseña incorrecta');
                limpiarCampos();
            }
        }
    });
}

function limpiarCampos() {
    $("#login").val("");
    $("#password").val("");
}