//Variables Globales
$listaProductos = {};
$listaSelect = new Array();
$tablaProductos = null;
$listaProveedores = {};
$listaCategorias = {};
$Img_P = null;

//Funcion de inicio
$(document).ready(function() {
    obtenerProveedores();
    obtenerCategorias();
    obtenerProductos();

});

//Manejo de check - global


//Manejo de check - individual
$(document).on('click', '#tbProductos tbody tr td:first-child', function(e) {
    e.preventDefault();
    var target;

    if (e.target.tagName == "LABEL") {
        target = $(e.target).parent().find(".indi-check")[0];
    } else if (e.target.tagName == "TD" || e.target.tagName == "DIV") {
        target = $(e.target).find(".indi-check")[0];

    }

    var tr = $(this).closest("tr");
    var rowindex = tr.index();

    buscarindex(rowindex);

    singleCheckHandler(target);
});

$(document).on('click', '#tbProductos thead tr th:first-child', function(e) {

    var table = $('#tbProductos').DataTable();

    var tableHead = $(this).closest("table")[0];
    var multiCheck = $(tableHead).find(".multi-check-cont input[type=checkbox]")[0];
    //Verificar que exista almenos un elemento en la tabla

    if (table.data().count() > 0) {

        if ($(multiCheck).prop('checked')) {
            $(multiCheck).prop('checked', false);
            recorrerTabla(table, "false");
            //Deshabilitar opciones
            $("#eliminar").prop("disabled", true);

        } else {
            $(multiCheck).prop('checked', true);
            recorrerTabla(table, "true");
            //Habilitar opciones
            $("#eliminar").prop("disabled", false);
            $("#eliminar").attr('onclick', 'eliminarProducto()');
        }
    }
});

function recorrerTabla($tabla, $accion) {
    $.each($listaProductos, function( index, value ) {
        if($accion == "true"){
            $("#indi-check-"+(value.Codigo_P)).prop('checked', true);
        }else{
            $("#indi-check-"+(value.Codigo_P)).prop('checked', false);
        }

    });
}

function buscarindex($index) {
    var result = false;
    var i = 0;

    for(var x = 0; x < $listaSelect.length; x++){
        if($listaSelect[x] == $index){
            result = true;
            i = x;
            x = $listaSelect.length;
        }
    }

    if (result){
        $listaSelect.splice(i,1);
    }else{
        $listaSelect.push($index);
    }
}

function singleCheckHandler(target) {

    //Con check, quitar check individual y global
    if (target.checked) {
        $(target).prop('checked', false);

        //Sin check, agregar check inidivual
    } else {
        $(target).prop('checked', true);
    }

    var tableBody = $(target).closest("table")[0];

    var totalChecks = $(tableBody).find(".indi-check").length;
    var checked = $(tableBody).find(".indi-check:checked").length;
    //Si todos los checks individuales estan activos, activar el global

    if ((totalChecks) == checked) {
        $("#multi-check").prop('checked', true);
    }else{
        $("#multi-check").prop('checked', false);
    }

    //Si hay mas de un check individual, activar la opcion de eliminar

    if ((checked) > 0) {
        $("#eliminar").prop("disabled", false);
        $("#eliminar").attr('onclick', 'eliminarProducto()');
    } else {
        $("#eliminar").prop("disabled", true);
    }
}

//Funcion obtener lista productos
function obtenerProductos(){  
    $.ajax({
        url: '../data/Producto.php',
        type: 'GET',
        data: {obtenerProductos: "true"},
        success: function (resultado) {
            $listaProductos = jQuery.parseJSON(resultado);
            inicializarTabla();    
        }
    });
}

//Funcion obtener lista proveedores
function obtenerProveedores(){  
    $.ajax({
        url: '../data/Proveedor.php',
        type: 'GET',
        data: {obtenerProveedores: "true"},
        success: function (resultado) {
            $listaProveedores = jQuery.parseJSON(resultado);
            var codigoProveedores = $listaProveedores[0].Id_P;
            inicializarSelectProveedores(codigoProveedores);    
        }
    });
}

//Funcion obtener el select del proveedor
function inicializarSelectProveedores(codigoProveedores){
    var html = "";
    for(var i = 0; i < $listaProveedores.length; i++){
        $id = $listaProveedores[i].Id_P;
        $nombre = $listaProveedores[i].Nombre_P;
        if($id == codigoProveedores){
            html += "<option selected value='"+$id+"'>"+$nombre+"</option>";
        }else{
            html += "<option value='"+$id+"'>"+$nombre+"</option>";
        }
    }
    $('#Proveedor_P').html(html);
}

//Funcion obtener lista categorias
function obtenerCategorias(){
    //prueba de codigo eliminar al terminar

    $.ajax({
        url: '../data/Categoria.php',
        type: 'GET',
        data: {obtenerCategoria: "true"},
        success: function (resultado) {
            $listaCategorias = jQuery.parseJSON(resultado);
            var codigoCategoria = $listaCategorias[0].Codigo_Ca;
            inicializarSelectCategorias(codigoCategoria);
        }
    });
}

//Funcion obtener el select del categoria
function inicializarSelectCategorias(codigoCategoria){
    var html = "";
    for(var i = 0; i < $listaCategorias.length; i++){
        $codigo = $listaCategorias[i].Codigo_Ca;
        $nombre = $listaCategorias[i].Nombre_Ca;
        if($codigo == codigoCategoria){
            html += "<option selected value='"+$codigo+"'>"+$nombre+"</option>";
        }else{
            html += "<option value='"+$codigo+"'>"+$nombre+"</option>";
        }
    }

    $('#Categoria_P').html(html);
}

//Funcion insertar producto
function insertarProducto(){

    $Nombre_P = $('#Nombre_P').val();
    $Descripcion_P = $('#Descripcion_P').val();
    $Categoria_P = parseInt($('#Categoria_P option:selected').val());
    $Proveedor_P = parseInt($('#Proveedor_P option:selected').val());
    $Cantidad_P = $('#Cantidad_P').val();
    $Precio_P = $('#Precio_P').val();

    $.ajax({
        url: '../data/Producto.php',
        type: 'POST',
        dataType: 'html',
        data: {insertarProducto: "true", Nombre_P: $Nombre_P, Descripcion_P: $Descripcion_P, Img_P: $Img_P, Categoria_P: $Categoria_P,
                                         Proveedor_P: $Proveedor_P, Cantidad_P: $Cantidad_P, Precio_P: $Precio_P},
        success: function (resultado) {
            if(resultado == 1){
                swal("Agregar", "Su producto " + $Nombre_P + "se agregó correctamente", "success");
                obtenerProductos();
                $('#modalGeneralProducto').modal('hide');
            }else{
                swal("Error", "Ocurrió un error al agregar el producto " + $Nombre_P, "error");
            }
        }
    });
}

//Funcion editar producto
function editarProducto(codigoProducto){
    
    $Nombre_P = $('#Nombre_P').val();
    $Descripcion_P = $('#Descripcion_P').val();
    $Categoria_P = parseInt($('#Categoria_P option:selected').val());
    $Proveedor_P = parseInt($('#Proveedor_P option:selected').val());
    $Cantidad_P = $('#Cantidad_P').val();
    $Precio_P = $('#Precio_P').val();

    $.ajax({
        url: '../data/Producto.php',
        type: 'POST',
        dataType: 'html',
        data: {editarProducto: "true", Codigo_P: codigoProducto,Nombre_P: $Nombre_P, Descripcion_P: $Descripcion_P, Img_P: $Img_P, Categoria_P: $Categoria_P,
                                       Proveedor_P: $Proveedor_P, Cantidad_P: $Cantidad_P, Precio_P: $Precio_P},
        success: function (resultado) {
            console.log(resultado);
            if(resultado == 1){
                swal("Editar", "Su producto " + $Nombre_P + "se editó correctamente", "success");
                obtenerProductos();
                $('#modalGeneralProducto').modal('hide');
            }else{
                swal("Error", "Ocurrió un error al editar el producto " + $Nombre_P, "error");
            }
        }
    });
}

//Funcion abrir modal Registrar
function abrirModalAgregar(){

    $('#tituloModal').text('Registrar Producto');
    $('#btnAccion').text('Agregar');
    $('#btnAccion').attr('onclick', 'insertarProducto()');

    limpiarCampos();

    var html =  "<label for='imagenProducto'>Agregar imagen:</label>"+ 
                "<input id='input-b6' name='input-b6' type='file' multiple>";

    $('#vizualizarImagen').html(html);

    $("#input-b6").fileinput({
        theme: 'fas',
        showUpload: false,
        uploadUrl:"http://localhost/procesarImg.php",
        dropZoneEnabled: false,
        maxFileCount: 1,

    }).on('fileuploaded', function(event, previewId, index, fileId) {
        $Img_P = previewId.response.location;
        console.log(previewId.response.location)
    });

    $('#modalGeneralProducto').modal('show');
}

//Funcion dejar los campos vacios
function limpiarCampos(){

    $('#Nombre_P').val("");
    $('#Descripcion_P').val("");
    $('#Precio_P').val("");
    $('#Cantidad_P').val("");

    var codigo = 0;
    inicializarSelectCategorias(codigo);
    inicializarSelectProveedores(codigo);

}

//Funcion abrir modal editar
function abrirModalEditar(codigoProducto){

    $('#tituloModal').text('Editar Producto');
    $('#btnAccion').text('Guardar cambios');
    $('#btnAccion').attr('onclick', 'editarProducto('+codigoProducto+')');

    limpiarCampos();

    $posicion = buscarProducto(codigoProducto);

    $('#Nombre_P').val($listaProductos[$posicion].Nombre_P);
    $('#Descripcion_P').val($listaProductos[$posicion].Descripcion_P);
    $('#Precio_P').val($listaProductos[$posicion].Precio_P);
    $('#Cantidad_P').val($listaProductos[$posicion].Cantidad_P);
    $Img_P = $listaProductos[$posicion].Img_P;

    inicializarSelectCategorias($listaProductos[$posicion].Categoria_P);
    inicializarSelectProveedores($listaProductos[$posicion].Proveedor_P);

    var html =  "<input id='input-b6' name='input-b6' type='file' multiple>";

    $('#vizualizarImagen').html(html);

    $("#input-b6").fileinput({
        theme: 'fas',
        showUpload: false,
        uploadUrl:"http://localhost/procesarImg.php",
        dropZoneEnabled: false,
        maxFileCount: 1,
        mainClass: "input-group-lg",
        initialPreviewAsData: true,
        initialPreview: [
            //Previsualizar la imagen seleccionada
            $Img_P
        ],
        initialPreviewConfig: [
            {caption: $listaProductos[$posicion].Nombre_P+".jpg", size: 329892, width: "120px", url: "{$url}", key: 1}
        ]
    }).on('fileuploaded', function(event, previewId, index, fileId) {
        $Img_P = previewId.response.location;
        console.log(previewId.response.location)
    });

    $('#modalGeneralProducto').modal('show');
}

//Funcion obtener el objeto producto
function buscarProducto(codigoProducto){

    $posicionProducto = 0;

    for(var i = 0; i < $listaProductos.length; i++){
        if( $listaProductos[i].Codigo_P == codigoProducto){
            $posicionProducto = i;
            i = $listaProductos.length
        }    
    }
    return $posicionProducto;
}

// funcion para obtener los nombres de proveedor y categoria que se muestran en la tabla
function obtenerNombre($lista, $id) {
    $nombre;
    $lista.forEach(function(value, index) {
        if (value.Id_P == $id) {
            $nombre = value.Nombre_P;
        } else if (value.Codigo_Ca == $id) {
            $nombre = value.Nombre_Ca;
        }
    });
    return $nombre;
}

//Se crea la tabla 
function inicializarTabla() { 
    if($tablaProductos != null){
        $tablaProductos.clear().destroy();
        $tablaProductos = null
    }
    $('[data-tooltip="tooltip"]').tooltip({ trigger: "hover" });

    $tablaProductos = $('#tbProductos').DataTable({
        destroy: true,
        "scrollx": true,
        data: $listaProductos,
        columns: [
            { data: null },
            { data: "Nombre_P" },
            { data: "Descripcion_P" },
            { data: null },
            { data: "Precio_P" },
            { data: "Cantidad_P" },
            { data: null },
            { title: "Editar" },
        ],
        "language": {"url": "json/configDatatable.json"},
        "columnDefs": [
        {
            "targets": 0,
            "width": "5%",
            "orderable": false,
            "className": "text-center",
            "render": function(data, type, row) {
                return (
                    '<div class="custom-control custom-checkbox indi-check-cont">' +
                    '<input type="checkbox" class="custom-control-input indi-check" id="indi-check-' + data.Codigo_P + '"/>' +
                    '<label class="custom-control-label ml-2" for="indi-check-' + data.Codigo_P + '"></label>' +
                    '</div>'
                );
            },
        },
        {
            "targets": 3,
            "width": "5%",
            "orderable": true,
            "className": "text-center",
            "render": function(data, type, row) {
                return (
                    '<div class="form-group">'+
                        '<label for="nombreCategoria">'+ obtenerNombre($listaCategorias, data.Categoria_P) +'</label>'+
                    '</div>'
                );
            },
        },
        {
            "targets": 6,
            "width": "5%",
            "orderable": true,
            "className": "text-center",
            "render": function(data, type, row) {
                return (
                    '<div class="form-group">'+
                        '<label for="nombreProveedor">'+ obtenerNombre($listaProveedores, data.Proveedor_P) +'</label>'+
                    '</div>'
                );
            },
        },   
        {
            "targets": 7, 
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {                                                                                                                                                                                     
                 return "<button id='editar' type='button' data-toggle='tooltip' data-placement='top' title='Editar selección' class='cont-icono btn btn-outline-succes' disabled><i class='fas fa-edit' onclick='abrirModalEditar(" + data.Codigo_P + ")'></i></button>";
            }
        }
        ],
        "order": [[0, "desc"]],
        "autoWidth": false,
        "preDrawCallback": function(settings) {
                $("#tbProductos tbody tr").removeClass("seleccionado");
                $("#tbProductos tbody tr td").removeClass("selected");
            },
        "drawCallback": function(settings) {
            $("#eliminar").prop("disabled", true);
            $("#tbProductos tbody tr").removeClass("selected");
            $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        }
    });
}

function eliminarProducto(){

    $titulo = "Esta Seguro de Eliminar los Productos seleccionados";
    $correcto = "Los productos seleccionados fueron eliminados";
    $error = "Ocurrió un fallo al eliminar los productos seleccionados";
    $cancelo = "Los productos seleccionados estan a salvo";

    if($listaSelect.length == 1){
        $titulo = "Esta Seguro de Eliminar el Producto " + $listaProductos[$listaSelect[0]].Nombre_P + "?";
        $correcto = "Su producto " + $listaProductos[$listaSelect[0]].Nombre_P + " fue eliminado correctamente";
        $error = "Ocurrió un fallo al eliminar el producto " + $listaProductos[$listaSelect[0]].Nombre_P;
        $cancelo = "Su producto " + $listaProductos[$listaSelect[0]].Nombre_P + " esta a salvo";
    }

    $codigos = "";
    if($("#multi-check").prop('checked')){
        $.each($listaProductos, function( index, value ) {
            $codigos += value.Codigo_P+",";
        });
    }else{
        $.each($listaSelect, function( index, value ) {
            $codigos += $listaProductos[value].Codigo_P+",";
        });
    }


    swal({
        title: $titulo,
        text: "Al eliminar este producto no se puede recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, Eliminarlo!",
        cancelButtonClass: "btn btn-secondary",
        cancelButtonText: "No, Cancelar!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
            $.ajax({
                url: '../data/Producto.php',
                type: 'POST',
                data: {eliminarProducto: "true", Codigo_P: $codigos},
                success: function (resultado) {
                    if(resultado == 1){
                       swal("Eliminó", $correcto,"success");
                        obtenerProductos();
                    }else{
                        swal("Error", $error , "error");
                    }          
                }
            });
      } else {
        swal("Canceló", $cancelo, "error");
      }
    });
}
