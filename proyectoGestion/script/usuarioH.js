//Variables Globales
$listaUsuarios = {};
$tablaUsuarios = null;

$( document ).ready(function() {
    obtenerUsuarios();
});

//Funcion obtener lista usuarios
function obtenerUsuarios(){

    $.ajax({
        url: '../data/Usuario.php',
        type: 'GET',
        data: {obtenerUsuarios: "true"},
        success: function (resultado) {
            $listaUsuarios = jQuery.parseJSON(resultado);
            inicializarTablaUsuarios();
        }
    });
}

//Funcion abrir modal Registrar usuario
function abrirModalRegistrarUsuario(){
    limpiarCamposUsuario();
    document.getElementById("cedulaUsuario").readOnly = false;
    $('#tituloModalUsuario').text('Registrar Usuario');
    $('#btnUsuario').text('Agregar');
    $('#btnUsuario').attr('onclick', 'registarUsuario()');
    $('#modalUsuario').modal('show');
}

//Funcion abrir modal modificar usuario
function abrirModalModificarUsuario($data) {
    $('#tituloModalUsuario').text('Modificar Usuario');
    $('#btnUsuario').text('Modificar');
    $('#btnUsuario').attr('onclick', 'modificarUsuario()');
    $('#modalUsuario').modal('show');

    $posicion = buscarUsuario($data);

    $('#cedulaUsuario').val($listaUsuarios[$posicion].Cedula_U);
    $('#nombreUsuario').val($listaUsuarios[$posicion].Nombre_U);
    $('#claveUsuario').val($listaUsuarios[$posicion].Password_U);
    if ($listaUsuarios[$posicion].Tipo_U == 1) {
        document.getElementById("tipoUsuario").checked = true;
    } else {
        document.getElementById("tipoUsuario").checked = false;
    }
    document.getElementById("cedulaUsuario").readOnly = true;
}

function registarUsuario() {

    $cedulaUsuario = $('#cedulaUsuario').val();
    $nombreUsuario = $('#nombreUsuario').val();
    $claveUsuario = $('#claveUsuario').val();
    //bool
    $tipoUsuario = Number(document.getElementById("tipoUsuario").checked);

    //verificar que no estén vacíos
    if ($cedulaUsuario == "" || $nombreUsuario == "" || $claveUsuario == "") {
        swal("Error", "Ocurrió un error al agregar el nuevo usuario en el sistema, es necesario llenar TODOS los datos!", "error");
    } else {
        $.ajax({
            url: '../data/Usuario.php',
            type: 'POST',
            dataType: 'html',
            data: {registrarUsuarios: "true", Cedula_U: $cedulaUsuario, Nombre_U: $nombreUsuario, Password_U: $claveUsuario, Tipo_U: $tipoUsuario},
            success: function (resultado) {
                if(resultado == 1){
                    limpiarCamposUsuario();
                    swal("Operación Exitosa", "Se ha generado un usuario para " + $nombreUsuario + " en el sistema!", "success");
                    obtenerUsuarios();
                    $('#modalUsuario').modal('hide');
                }else{
                    swal("Error", "Ocurrió un error al agregar el nuevo usuario en el sistema!", "error");
                }
            }
        });

    }
}

function modificarUsuario() {
    $cedulaUsuario = $('#cedulaUsuario').val();
    $nombreUsuario = $('#nombreUsuario').val();
    $claveUsuario = $('#claveUsuario').val();
    //bool
    $tipoUsuario = Number(document.getElementById("tipoUsuario").checked);

    //verificar que no estén vacíos
    if ($cedulaUsuario == "" || $nombreUsuario == "" || $claveUsuario == "") {
        swal("Error", "Ocurrió un error al modificar usuario en el sistema, es necesario llenar TODOS los datos!", "error");
    } else {
        $.ajax({
            url: '../data/Usuario.php',
            type: 'POST',
            dataType: 'html',
            data: {modificararUsuarios: "true", Cedula_U: $cedulaUsuario, Nombre_U: $nombreUsuario, Password_U: $claveUsuario, Tipo_U: $tipoUsuario},
            success: function (resultado) {
                if(resultado == 1){
                    limpiarCamposUsuario();
                    swal("Operación Exitosa", "Se ha modificado la información del usuario " + $nombreUsuario + " en el sistema!", "success");
                    obtenerUsuarios();
                    $('#modalUsuario').modal('hide');
                }else{
                    swal("Error", "Ocurrió un error al modificar el usuario en el sistema!", "error");
                }
            }
        });

    }
}
        
function eliminarUsuario($cedula) {
    // obtener el index de la lista del usuario a eliminar
    $posicion = buscarUsuario($cedula);

    swal({
        title: "Confirmar Operación",
        text: "Al confirmar se eliminará al usuario '" + $listaUsuarios[$posicion].Nombre_U +"' del sistema, esta operación no es reversible!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            // debe enviar a eliminar en backend {bd}
            $.ajax({
                url: '../data/Usuario.php',
                type: 'POST',
                data: {eliminarUsuarios: "true", Cedula_U: $cedula},
                success: function (resultado) {
                    if(resultado == 1){

                        swal("Operación Exitosa", "El usuario '" + $listaUsuarios[$posicion].Nombre_U + "'' fue eliminado correctamente del sistema!", "success");
                        obtenerUsuarios();
                    }else{
                        swal("Error", "Ocurrió un fallo al eliminar el producto " + $listaUsuarios[$posicion].Nombre_U , "error");
                    }
                }
            });

        } else {
            swal("Operación Cancelada", "No se ha eliminado al usuario del sistema!", "success");
        }
    });
}

//Funcion obtener el objeto producto
function buscarUsuario(cedula){

    $posicionProducto = 0;

    for(var i = 0; i < $listaUsuarios.length; i++){
        if( $listaUsuarios[i].Cedula_U == cedula){
            $posicionProducto = i;
            i = $listaUsuarios.length
        }
    }
    return $posicionProducto;
}

function limpiarCamposUsuario(){
    $('#cedulaUsuario').val("");
    $('#nombreUsuario').val("");
    $('#claveUsuario').val("");
    document.getElementById("tipoUsuario").checked = false;
}

//Se crea la tabla 
function inicializarTablaUsuarios() { 
    if($tablaUsuarios != null){
        $tablaUsuarios.clear().destroy();
        $tablaUsuarios = null
    }

    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });

    $tablaUsuarios = $('#tbUsuarios').DataTable({
        destroy: true,
        "scrollx": true,
        "lengthMenu": [ 4 ],
        data: $listaUsuarios,
        columns: [
            { title: "Cédula", data: "Cedula_U" },
            { title: "Nombre", data: "Nombre_U"  },
            { title: "Contraseña", data: "Password_U"  },
            { title: "Editar" },
            { title: "Eliminar" },
        ],
        "language": {"url": "../json/configDatatable.json"},
        "columnDefs": [ 
        {
            "targets": 3, 
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {                                                                                                                                                                                     
                 return "<button id='modificarUsuario' type='button' data-toggle='tooltip' data-placement='top' title='Modificar selección' class='cont-icono btn btn-outline-succes'><i class='fas fa-edit' onclick='abrirModalModificarUsuario(" + data.Cedula_U + ")'></i></button>";
            }
        },
        {
            "targets": 4, 
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                 return "<button id='eliminarUsuario' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger'><i class='far fa-trash-alt' onclick='eliminarUsuario("+ data.Cedula_U +")'></i></button>"
            }
        }
        ],
        "order": [[0, "asc"]],
        "autoWidth": false,
        "preDrawCallback": function(settings) {
                $("#tbUsuarios tbody tr").removeClass("seleccionado");
                $("#tbUsuarios tbody tr td").removeClass("selected");
            },
        "drawCallback": function(settings) {
            //$("#eliminarUsuario").prop("disabled", true);
            $("#tbUsuarios tbody tr").removeClass("selected");
            $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        }
    });
}
