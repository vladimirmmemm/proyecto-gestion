$listaClientes = {};
$listaSelect = new Array();

$tablaClientes = null;

//Funcion de inicio
$(document).ready(function() {
    obtenerClientes();
});

function obtenerClientes(){
    $.ajax({
        url: '../data/Cliente.php',
        type: 'GET',
        data: {obtenerClientes: "true"},
        success: function (resultado) {
            $listaClientes = jQuery.parseJSON(resultado);
            inicializarTabla();
        }
    });
}

//Funcion abrir modal Registrar
function abrirModalAgregar(){

    $('#tituloModal').text('Registrar Producto');
    $('#btnAccion').text('Agregar');
    $('#btnAccion').attr('onclick', 'insertarProducto()');

    limpiarCampos();
    $('#Correo_C').prop("disabled", false);

    $('#modalGeneralProducto').modal('show');
}

function limpiarCampos(){

    $('#Correo_C').val("");
    $('#Nombre_C').val("");
    $('#telefono_C').val("");
    $('#password_C').val("");

}

function insertarProducto(){

    var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

    if (regex.test($('#Correo_C').val().trim())) {

        $correo = $('#Correo_C').val();
        $nombre = $('#Nombre_C').val();
        $tel = $('#telefono_C').val();
        $pass = $('#password_C').val();

        if($correo != "" && $nombre != "" && $tel != "" && $pass != "") {
            $.ajax({
                url: '../data/Cliente.php',
                type: 'POST',
                dataType: 'html',
                data: {registrarClientes: "true", Correo_C: $correo, Nombre_C: $nombre, Telefono_C: $tel, Password_C: $pass},
                success: function (resultado) {

                    if(resultado == 1){
                        swal("Agregar", "Cliente " + $nombre + " agregado correctamente", "success");
                        obtenerClientes();
                        $('#modalGeneralProducto').modal('hide');
                    }else{
                        swal("Error", "Ocurrio un error al Agregar el cliente " + $nombre, "error");
                    }
                }
            });
        }else{
            alertify.error('Se encontraron campos en blanco.');
        }

    } else {
        alertify.error('La direccón de correo no es válida');
    }
}

function eliminarCliente(){

    $titulo = "Esta Seguro de Eliminar los Productos seleccionados";
    $correcto = "Los productos seleccionados fueron eliminados";
    $error = "Ocurrió un fallo al eliminar los productos seleccionados";
    $cancelo = "Los productos seleccionados están a salvo";

    if($listaSelect.length == 1){
        $titulo = "Esta Seguro de Eliminar el cliente " + $listaClientes[$listaSelect[0]].Nombre_C + "?";
        $correcto = "Su producto " + $listaClientes[$listaSelect[0]].Nombre_C + " fue eliminado correctamente";
        $error = "Ocurrió un fallo al eliminar el producto " + $listaClientes[$listaSelect[0]].Nombre_C;
        $cancelo = "Su producto " + $listaClientes[$listaSelect[0]].Nombre_C + " esta a salvo";
    }

    $codigos = "";
    if($("#multi-check").prop('checked')){
        $.each($listaClientes, function( index, value ) {
            $codigos += value.Correo_C+",";
        });
    }else{
        $.each($listaSelect, function( index, value ) {
            $codigos += $listaClientes[value].Correo_C+",";
        });
    }

    swal({
            title: $titulo,
            text: "Al eliminar este producto no se puede recuperar!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Sí, Eliminarlo!",
            cancelButtonClass: "btn btn-secondary",
            cancelButtonText: "No, Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '../data/Cliente.php',
                    type: 'POST',
                    data: {eliminarCliente: "true", Correo_C: $codigos},
                    success: function (resultado) {
                        if(resultado == 1){
                            swal("Elimino", $correcto,"success");
                            obtenerClientes();
                        }else{
                            swal("Error", $error , "error");
                        }
                    }
                });
            } else {
                swal("Canceló", $cancelo, "error");
            }
        });
}

function abrirModalEditar($posicion){

    $('#tituloModal').text('Editar Cliente');
    $('#btnAccion').text('Guardar cambios');
    $('#btnAccion').attr('onclick', 'editarCliente()');

    limpiarCampos();

    $('#Correo_C').val($listaClientes[$posicion].Correo_C);
    $('#Nombre_C').val($listaClientes[$posicion].Nombre_C);
    $('#telefono_C').val($listaClientes[$posicion].Telefono_C);
    $('#password_C').val($listaClientes[$posicion].Password_C);

    $('#Correo_C').prop("disabled", true);

    $('#modalGeneralProducto').modal('show');
}

//Funcion obtener el objeto producto
function buscarCliente(codigoProducto){

    $posicionProducto = 0;

    for(var i = 0; i < $listaClientes.length; i++){

        if( $listaClientes[i].Correo_C == codigoProducto){
            $posicionProducto = i;
            i = $listaClientes.length;
        }
    }
    return $posicionProducto;
}

//Funcion editar producto
function editarCliente(){

    $correo = $('#Correo_C').val();
    $nombre = $('#Nombre_C').val();
    $tel = $('#telefono_C').val();
    $pass = $('#password_C').val();

    if($correo != "" && $nombre != "" && $tel != "" && $pass != "") {
        $.ajax({
            url: '../data/Cliente.php',
            type: 'POST',
            dataType: 'html',
            data: {modificarCliente: "true", Correo_C: $correo, Nombre_C: $nombre, Telefono_C: $tel, Password_C: $pass},
            success: function (resultado) {

                if(resultado == 1){
                    swal("Agregar", "Cliente " + $nombre + " agregado correctamente", "success");
                    obtenerClientes();
                    $('#modalGeneralProducto').modal('hide');
                }else{
                    swal("Error", "Ocurrió un error al Agregar el cliente " + $nombre, "error");
                }
            }
        });
    }else{
        alertify.error('Se encontraron campos en blanco.');
    }
}

function recuperarClave(Correo_C) {
    swal({
            title: "Confirmar Operación",
            text: "Al confirmar, se enviará  al correo del cliente una nueva contraseña de acceso!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, Confirmar!',
            cancelButtonText: 'No, Cancelar!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '../data/Cliente.php',
                    type: 'POST',
                    data: {modificarClaveCliente: "true", Correo_C: Correo_C},
                    success: function (resultado) {
                        if(resultado == 1){
                            swal("Operación Exitosa", "Se ha enviado al correo '"+ Correo_C +"' una nueva contraseña de acceso!","success");
                            obtenerClientes();
                        }else{
                            swal("Error", "Ha ocurrido error al intentar restablecer la contraseña del cliente" , "error");
                        }
                    }
                });
            } else {
                swal("Operación Cancelada", "NO se ha restablecido la contraseña del cliente", "success");
            }
        });
}

function inicializarTabla() {
    if($tablaClientes != null){
        $tablaClientes.clear().destroy();
        $tablaClientes = null;
    }
    $('[data-tooltip="tooltip"]').tooltip({ trigger: "hover" });

    $tablaClientes = $('#tbCliente').DataTable({
        destroy: true,
        "scrollx": true,
        data: $listaClientes,
        columns: [
            { data: null },
            { data: "Correo_C" },
            { data: "Nombre_C" },
            { data:  "Telefono_C"},
            { title: "Recuperar Contraseña" },
            { title: "Editar" },
        ],
        "language": {"url": "../json/configDatatable.json"},
        "columnDefs": [
            {
                "targets": 0,
                "width": "2%",
                "orderable": false,
                "className": "text-center",
                "render": function(data, type, row) {
                    return (
                        '<div class="custom-control custom-checkbox indi-check-cont">' +
                        '<input type="checkbox" class="custom-control-input indi-check" id="indi-check-' + ((data.Correo_C).replace("@", "-")).replace(".", "-") + '"/>' +
                        '<label class="custom-control-label ml-2" for="indi-check-' + ((data.Correo_C).replace("@", "-")).replace(".", "-")  + '"></label>' +
                        '</div>'
                    );
                },
            },
            {
                "targets": 4,
                "data": null,
                "orderable": false,
                "width": "5%",
                "className": "text-center bg-white",
                "mData": function (data, type, val) {
                    return "<button id='recuperar' type='button' data-toggle='tooltip' data-placement='top' title='Recuperar contraseña' class='cont-icono btn btn-outline-succes'><i class='fas fa-user-cog' onclick='recuperarClave("+ JSON.stringify(data.Correo_C) +");'></i></button>";
                }
            },
            {
                "targets": 5,
                "data": null,
                "orderable": false,
                "width": "5%",
                "className": "text-center bg-white",
                "mData": function (data, type, val) {
                    return "<button id='editar' type='button' data-toggle='tooltip' data-placement='top' title='Editar selección' class='cont-icono btn btn-outline-succes'><i class='fas fa-edit' onclick='abrirModalEditar("+ buscarCliente(data.Correo_C) +");'></i></button>";
                }
            }
        ],
        "order": [[0, "desc"]],
        "autoWidth": true,
        "preDrawCallback": function(settings) {
            $("#tbCliente tbody tr").removeClass("seleccionado");
            $("#tbCliente tbody tr td").removeClass("selected");
        },
        "drawCallback": function(settings) {
            $("#eliminar").prop("disabled", true);
            $("#tbCliente tbody tr").removeClass("selected");
            $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        }
    });
}

//Manejo de check - individual
$(document).on('click', '#tbCliente tbody tr td:first-child', function(e) {
    e.preventDefault();
    var target;

    if (e.target.tagName == "LABEL") {
        target = $(e.target).parent().find(".indi-check")[0];
    } else if (e.target.tagName == "TD" || e.target.tagName == "DIV") {
        target = $(e.target).find(".indi-check")[0];

    }

    var tr = $(this).closest("tr");
    var rowindex = tr.index();

    buscarindex(rowindex);

    singleCheckHandler(target);
});

function singleCheckHandler(target) {
    //Con check, quitar check individual y global
    if (target.checked) {
        $(target).prop('checked', false);

        //Sin check, agregar check inidivual
    } else {
        $(target).prop('checked', true);
    }

    var tableBody = $(target).closest("table")[0];

    var totalChecks = $(tableBody).find(".indi-check").length;
    var checked = $(tableBody).find(".indi-check:checked").length;
    //Si todos los checks individuales estan activos, activar el global

    if ((totalChecks) == checked) {
        $("#multi-check").prop('checked', true);
    }else{
        $("#multi-check").prop('checked', false);
    }

    //Si hay mas de un check individual, activar la opcion de eliminar
    if ((checked) > 0) {
        $("#eliminar").prop("disabled", false);
        $("#eliminar").attr('onclick', 'eliminarCliente()');
    } else {
        $("#eliminar").prop("disabled", true);
    }
}

function buscarindex($index) {
    var result = false;
    var i = 0;

    for(var x = 0; x < $listaSelect.length; x++){
        if($listaSelect[x] == $index){
            result = true;
            i = x;
            x = $listaSelect.length;
        }
    }

    if (result){
        $listaSelect.splice(i,1);
    }else{
        $listaSelect.push($index);
    }
}

$(document).on('click', '#tbCliente thead tr th:first-child', function(e) {

    var table = $('#tbCliente').DataTable();

    var tableHead = $(this).closest("table")[0];
    var multiCheck = $(tableHead).find(".multi-check-cont input[type=checkbox]")[0];
    //Verificar que exista almenos un elemento en la tabla

    if (table.data().count() > 0) {

        if ($(multiCheck).prop('checked')) {
            $(multiCheck).prop('checked', false);
            recorrerTabla("false");
            //Deshabilitar opciones
            $("#eliminar").prop("disabled", true);

        } else {
            $(multiCheck).prop('checked', true);
            recorrerTabla("true");
            //Habilitar opciones
            $("#eliminar").prop("disabled", false);
            $("#eliminar").attr('onclick', 'eliminarCliente()');
        }
    }
});

function recorrerTabla($accion) {
    $.each($listaClientes, function( index, value ) {
        if($accion == "true"){
            $("#indi-check-"+((value.Correo_C).replace("@", "-")).replace(".", "-") ).prop('checked', true);
        }else{
            $("#indi-check-"+((value.Correo_C).replace("@", "-")).replace(".", "-") ).prop('checked', false);
        }

    });
}