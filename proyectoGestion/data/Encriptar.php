<?php
require_once "../coneccion/conexion.php";
class Encriptar extends Conexion {

	public function encriptarClave($clave) {
		return password_hash($clave, PASSWORD_DEFAULT, ['cost' => 15]);
	}

	public function verificarClave($clave, $claveBD) {
		if (password_verify($clave, $claveBD)) {
			return $clave;
		} else {
			return 0;
		}
	}
}

?>