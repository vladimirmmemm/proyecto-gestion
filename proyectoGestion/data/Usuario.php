<?php

require_once "Encriptar.php";
session_start();

class Usuario extends Encriptar {

    public function obtenerUsuarios() {

        try {
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare( "call sodaonline.lista_Usuario();" );
            $conexion->execute();
            $conexion->bind_result($Cedula_U,$Nombre_U,$Password_U,$Tipo_U);

            $listaUsuarios = array();

            while ($conexion->fetch()) {

                $usuario = (object) [
                    'Cedula_U' => $Cedula_U,
                    'Nombre_U' => $Nombre_U,
                    'Password_U' => $Password_U,
                    'Tipo_U' => $Tipo_U
                ];

                array_push($listaUsuarios, $usuario);
            }

            mysqli_close($this->getConexion());

            return json_encode($listaUsuarios);

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function registrarUsuarios($Cedula_U,$Nombre_U,$Password_U,$Tipo_U) {

        $clave = $this->encriptarClave($Password_U);

        try{
            $sentencia = $this->getConexion()->prepare("CALL insertar_Usuario(?,?,?,?); ");
            $sentencia->bind_param('sssi',$Cedula_U,$Nombre_U,$clave,$Tipo_U);
            $resultado=$sentencia->execute();

            $sentencia->close();

            $this->cerrarConexion();
            return $resultado;

        }catch (Exception $ex){
            return $ex;

        }
    }


    public function eliminarUsuario($Cedula_U){

        try{

            $sentencia = $this->getConexion()->prepare("CALL eliminar_Usuario(?); ");
            $sentencia->bind_param('s',$Cedula_U );
            $resultado = $sentencia->execute();
            $sentencia->close();
            $this->cerrarConexion();
            return $resultado;
        }catch (Exception $ex){
            return $ex;
        }

    }

    public function modificarUsuario($Cedula_U,$Nombre_U,$Password_U,$Tipo_U){
        $clave = $this->encriptarClave($Password_U);
            try{
                $sentencia = $this->getConexion()->prepare("CALL editar_Usuario(?,?,?,?); ");
                $sentencia->bind_param('sssi',$Cedula_U,$Nombre_U,$clave,$Tipo_U);
                $resultado = $sentencia->execute();
                $sentencia->close();
                $this->cerrarConexion();
                
                return $resultado;
                if ($resultado->affected_rows == 1) {
                    echo 1; 
                }
                else {
                    echo 0;
                }
            }catch(Exception $ex){
                return $ex;
            }

    }
    public function obtenerUsuarioLogin($Cedula_U,$Clave) {

        try {
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare( "call sodaonline.obtenerUsuario(?);" );
            $conexion->bind_param("s",$Cedula_U);
            $conexion->execute();
            $conexion->bind_result($Cedula_U,$Nombre_U, $Password_U);

            $listaUsuario = array();

            while ($conexion->fetch()) {
                $claveUsuario =  $this->verificarClave($Clave, $Password_U);
                $Usuario = (object) [
                    'Cedula_U' => $Cedula_U,
                    'Nombre_U' => $Nombre_U,
                    'Password_U' => $claveUsuario
                ];

                array_push($listaUsuario, $Usuario);
            }

            return json_encode($listaUsuario);

        } catch (Exception $ex) {
            return $ex;
        }finally {
            mysqli_close($this->getConexion());
        }
    }

}

$usuario = new Usuario();

if ( (isset($_REQUEST['obtenerUsuarios'])) ) {
    echo $usuario->obtenerUsuarios();
}
if ( (isset($_REQUEST['registrarUsuarios'])) ) {
    echo $usuario->registrarUsuarios( $_POST['Cedula_U'],$_POST['Nombre_U'],$_POST['Password_U'],$_POST['Tipo_U']);
}
if ( (isset($_REQUEST['eliminarUsuarios'])) ) {
    echo $usuario->eliminarUsuario( $_POST['Cedula_U']);
}
if ( (isset($_REQUEST['modificararUsuarios'])) ) {
    echo $usuario->modificarUsuario( $_POST['Cedula_U'],$_POST['Nombre_U'],$_POST['Password_U'],$_POST['Tipo_U']);
}
if ( (isset($_REQUEST['obtenerUsuarioLogin'])) ) {
    echo $usuario->obtenerUsuarioLogin( $_REQUEST['Cedula_U'],$_REQUEST['Password_U']);
}



