<?php
require_once '../coneccion/conexion.php';

session_start();

class Categoria extends conexion{
  
    public function obtenerCategoria() {

        try {
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare( "call sodaonline.lista_Categoria();" );
            $conexion->execute();
            $conexion->bind_result($Codigo_Ca,$Nombre_Ca);

            $listaCategoria = array();

            while ($conexion->fetch()) {

                $categoria = (object) [
                    'Codigo_Ca' => $Codigo_Ca,
                    'Nombre_Ca' => utf8_encode($Nombre_Ca)
                ];

                array_push($listaCategoria, $categoria);
            }
            //var_dump(json_encode($listaCategoria));
            return json_encode($listaCategoria);

        } catch (Exception $ex) {
            return $ex;
        }finally {
            mysqli_close($this->getConexion());
        }
    }

    public function insertarCategoria($Nombre_Ca) {

        try{
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare("CALL insertar_Categoria(?); ");
            $conexion->bind_param('s',$Nombre_Ca);
            $conexion->execute();

            if ($conexion->affected_rows == 1) {
                return 1;
            }
            else {
                return 0;
            }
        }catch (Exception $ex){
            return $ex;

        } finally {
            mysqli_close($this->getConexion());
        }
    }

    public function eliminarCategoria($Codigo_Ca){
        $lista = explode(",",$Codigo_Ca);
        try{
            $conexion = $this->getConexion()->stmt_init();
            foreach ($lista as $codigo){
                if ((int)$codigo != 0) {
                    $conexion->prepare("CALL eliminar_Categoria(?);");
                    $conexion->bind_param('i',$codigo);
                    $conexion->execute();
                }
            }
            // pruebas unitarias -> verificar si insertó
            if ($conexion->affected_rows == 1) {
                return 1;
            }
            else {
                return 0;
            }
        }catch (Exception $ex){
            return $ex;
        } finally {
            mysqli_close($this->getConexion());
        }
    }

    public function modificarCategoria($Codigo_Ca,$Nombre_Ca){

        try{
            $conexion = $this->getConexion()->stmt_init();

            $conexion->prepare("CALL sodaonline.editar_Categoria(?,?); ");
            $conexion->bind_param('is',$Codigo_Ca,$Nombre_Ca);
            $conexion->execute();

            return 1;
        }catch(Exception $ex){
            return $ex;
        }finally {
            mysqli_close($this->getConexion());
        }

    }
}

$categoria = new Categoria();

if ( (isset($_GET['obtenerCategoria'])) ) {
    echo $categoria->obtenerCategoria();
}

if ( (isset($_POST['eliminarCategoria'])) ) {
    echo $categoria->eliminarCategoria( $_POST['Codigo_Ca']);
}

if ( (isset($_POST['insertarCategoria'])) ) {
    echo $categoria->insertarCategoria( $_POST['Nombre_Ca']);
}

if ( (isset($_POST['modificarCategoria'])) ) {
    echo $categoria->modificarCategoria( $_POST['Codigo_Ca'],$_POST['Nombre_Ca']);
}
