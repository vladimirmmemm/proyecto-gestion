<?php
require_once '../coneccion/conexion.php';

session_start();

class Proveedor extends conexion {

	public function insertarProveedor($Nombre_P, $Correo_P, $Telefono_P) {
        try{
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare("CALL insertar_Proveedor(?,?,?);");
            $conexion->bind_param('sss',$Nombre_P, $Correo_P, $Telefono_P);
            $conexion->execute();
            // pruebas unitarias -> verificar si insertó
            if ($conexion->affected_rows == 1) {
                return 1;
            }
            else {
                return 0;
            }
        }catch (Exception $ex){
            return $ex;
        } finally {
            mysqli_close($this->getConexion());
        }
    }

    public function modificarProveedor($Id_P, $Nombre_P, $Correo_P, $Telefono_P){
        try{
            $conexion = $this->getConexion()->stmt_init();

            $conexion->prepare("CALL editar_Proveedor(?,?,?,?); ");
            $conexion->bind_param('isss',$Id_P, $Nombre_P, $Correo_P, $Telefono_P);
            $conexion->execute();

            // pruebas unitarias -> verificar si insertó
            if ($conexion->affected_rows == 1) {
                return 1;
            }
            else {
                return 0;
            }
        }catch(Exception $ex){
            return $ex;
        }finally {
            mysqli_close($this->getConexion());
        }
    }

    public function obtenerProveedores() {
        try {
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare("call sodaonline.lista_Proveedor();");
            $conexion->execute();
            $conexion->bind_result($Id_P, $Nombre_P, $Correo_P, $Telefono_P);

            $listaProveedores = array();

            while ($conexion->fetch()) {
                $proveedor = (object) [
                    'Id_P' => $Id_P,
                    'Nombre_P' => utf8_encode($Nombre_P),
                    'Correo_P' => utf8_encode($Correo_P),
                    'Telefono_P' => utf8_encode($Telefono_P)
                ];
                array_push($listaProveedores, $proveedor);
            }
            return json_encode($listaProveedores);

        } catch (Exception $ex) {
            return $ex;
        }finally {
            mysqli_close($this->getConexion());
        }
    }

    public function eliminarProveedor($Id_P){
        $lista = explode(",",$Id_P);
        try{
            $conexion = $this->getConexion()->stmt_init();
            foreach ($lista as $id){
                if ((int)$id != 0) {
                    $conexion->prepare("CALL eliminar_Proveedor(?);");
                    $conexion->bind_param('i',$id);
                    $conexion->execute();
                }
            }
            if ($conexion->affected_rows >= 1) {
                return 1;
            }
            else {
                return 0;
            }
        }catch (Exception $ex){
            return $ex;
        } finally {
            mysqli_close($this->getConexion());
        }
    }
}

$proveedor = new Proveedor();

if ( (isset($_POST['insertarProveedor'])) ) {
    echo $proveedor->insertarProveedor($_POST['Nombre_P'] , $_POST['Correo_P'],$_POST['Telefono_P']);
}

if ( (isset($_POST['modificarProveedor'])) ) {
    echo $proveedor->modificarProveedor($_POST['Id_P'], $_POST['Nombre_P'], $_POST['Correo_P'], $_POST['Telefono_P']);
}

if ( (isset($_GET['obtenerProveedores'])) ) {
    echo $proveedor->obtenerProveedores();
}

if ( (isset($_POST['eliminarProveedor'])) ) {
    echo $proveedor->eliminarProveedor( $_POST['Id_P']);
}