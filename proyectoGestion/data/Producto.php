<?php
require_once '../coneccion/conexion.php';

session_start();

class Producto extends conexion{
  
    public function obtenerProductos() {

        try {
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare( "call sodaonline.lista_Producto();" );
            $conexion->execute();
            $conexion->bind_result($Codigo_P,$Nombre_P,$Descripcion_P,$Img_P,$Categoria_P,$Proveedor_P,$Cantidad_P,$Precio_P);

            $listaProductos = array();

            while ($conexion->fetch()) {

                $producto = (object) [
                    'Codigo_P' => $Codigo_P,
                    'Nombre_P' => $Nombre_P,
                    'Descripcion_P' => $Descripcion_P,
                    'Img_P' => $Img_P,
                    'Categoria_P' => $Categoria_P,
                    'Proveedor_P' => $Proveedor_P,
                    'Cantidad_P' => $Cantidad_P,
                    'Precio_P' => $Precio_P
                ];
                //var_dump($listaProductos);
                array_push($listaProductos, $producto);
            }

            return json_encode($listaProductos);

        } catch (Exception $ex) {
            return $ex;
        }finally {
            mysqli_close($this->getConexion());
        }
    }

    public function insertarProducto($Nombre_P,$Descripcion_P,$Img_P,$Categoria_P,$Proveedor_P,$Cantidad_P,$Precio_P) {

        try{
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare("CALL insertar_Producto(?,?,?,?,?,?,?); ");
            $conexion->bind_param('sssssss',$Nombre_P,$Descripcion_P,$Img_P,$Categoria_P,$Proveedor_P,$Cantidad_P,$Precio_P);
            $conexion->execute();
            // pruebas unitarias -> verificar si insertó

            if ($conexion->affected_rows >= 1) {
                return 1;
            }
            else {
                return 0;
            }
        }catch (Exception $ex){
            return $ex;

        } finally {
            mysqli_close($this->getConexion());
        }
    }

    public function eliminarProducto($Codigo_P){
        $lista = explode(",",$Codigo_P);
        try{
            $conexion = $this->getConexion()->stmt_init();
            foreach ($lista as $codigo){
                if ((int)$codigo != 0) {
                    $conexion->prepare("CALL eliminar_Producto(?);");
                    $conexion->bind_param('i',$codigo);
                    $conexion->execute();
                }
            }
            if ($conexion->affected_rows >= 1) {
                return 1;
            }
            else {
                return 0;
            }
        }catch (Exception $ex){
            return $ex;
        } finally {
            mysqli_close($this->getConexion());
        }
    }

    public function modificarProducto($Codigo_P,$Nombre_P,$Descripcion_P,$Img_P,$Categoria_P,$Proveedor_P,$Cantidad_P,$Precio_P){

        try{
            $conexion = $this->getConexion()->stmt_init();

            $conexion->prepare("CALL editar_Producto(?,?,?,?,?,?,?,?); ");
            $conexion->bind_param('isssiiii',$Codigo_P,$Nombre_P,$Descripcion_P,$Img_P,$Categoria_P,$Proveedor_P,$Cantidad_P,$Precio_P);
            $conexion->execute();

            // pruebas unitarias -> verificar si insertó
            if ($conexion->affected_rows >= 1) {
                return 1;
            }
            else {
                return 0;
            }
        }catch(Exception $ex){
            return $ex;
        }finally {
            mysqli_close($this->getConexion());
        }

    }

    public function obtenerUltimoInsertado() {
        try{
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare( "CALL sodaonline.obtenerUltimoProductoInsertado();" );
            $conexion->execute();
            $conexion->bind_result($Codigo_P);
            if ($conexion->fetch()) {
                return $Codigo_P;
            }
        }catch (Exception $ex){
            return $ex;
        }
    }

}

$producto = new Producto();

if ( (isset($_GET['obtenerProductos'])) ) {
    echo $producto->obtenerProductos();
}

if ( (isset($_POST['eliminarProducto'])) ) {
    echo $producto->eliminarProducto( $_POST['Codigo_P']);
}

if ( (isset($_POST['insertarProducto'])) ) {
    echo $producto->insertarProducto( $_POST['Nombre_P'] , $_POST['Descripcion_P'],$_POST['Img_P'] ,
        $_POST['Categoria_P'], $_POST['Proveedor_P'], $_POST['Cantidad_P'], $_POST['Precio_P']);
}

if ( (isset($_POST['editarProducto'])) ) {
    echo $producto->modificarProducto( $_POST['Codigo_P'],$_POST['Nombre_P'] , $_POST['Descripcion_P'],$_POST['Img_P'] ,
        $_POST['Categoria_P'], $_POST['Proveedor_P'], $_POST['Cantidad_P'], $_POST['Precio_P']);
}
