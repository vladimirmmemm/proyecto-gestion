<?php

require_once "Encriptar.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../vendor/autoload.php';

session_start();

class Cliente extends Encriptar {

    public function obtenerClientes() {

        try {
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare( "call sodaonline.lista_Cliente();" );
            $conexion->execute();
            $conexion->bind_result($Correo_C,$Nombre_C,$Telefono_C,$Password_C);

            $listaClientes = array();

            while ($conexion->fetch()) {

                $Cliente = (object) [
                    'Correo_C' => $Correo_C,
                    'Nombre_C' => $Nombre_C,
                    'Telefono_C' => $Telefono_C,
                    'Password_C' => $Password_C
                ];

                array_push($listaClientes, $Cliente);
            }

            return json_encode($listaClientes);

        } catch (Exception $ex) {
            return $ex;
        }finally {
            mysqli_close($this->getConexion());
        }
    }

    public function registrarClientes($Correo_C,$Nombre_C,$Telefono_C,$Password_C) {
        $clave = $this->encriptarClave($Password_C);
        try{
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare("CALL insertar_Cliente(?,?,?,?); ");
            $conexion->bind_param('ssss',$Correo_C,$Nombre_C,$Telefono_C,$clave);
            $conexion->execute();

            if ($conexion->affected_rows >= 1) {
                return 1;
            }
            else {
                return 0;
            }
        }catch (Exception $ex){
            return $ex;

        }finally {
            mysqli_close($this->getConexion());
        }
    }


    public function eliminarCliente($Correo_C){
        $lista = explode(",",$Correo_C);
        try{
            $conexion = $this->getConexion()->stmt_init();
            foreach ($lista as $codigo){
                $conexion->prepare("CALL eliminar_Cliente(?);");
                $conexion->bind_param('s',$codigo);
                $conexion->execute();
            }

            return 1;
        }catch (Exception $ex){
            return $ex;
        } finally {
            mysqli_close($this->getConexion());
        }

    }

    public function modificarCliente($Correo_C,$Nombre_C,$Telefono_C,$Password_C){
        $clave = $this->encriptarClave($Password_C);
        try{
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare("CALL editar_Cliente(?,?,?,?); ");
            $conexion->bind_param('ssss',$Correo_C,$Nombre_C,$Telefono_C,$clave);
            $conexion->execute();

            if ($conexion->affected_rows >= 1) {
                return 1;
            }
            else {
                return 0;
            }

        }catch (Exception $ex){
            return $ex;

        }finally {
            mysqli_close($this->getConexion());
        }
    }

    private function generarClaveAleatoria($correo) {
        $clave = "";
        for ($i = 0; $i <= 5; $i++) {         
           $clave .= $correo[rand (0,6)];
        }
        return $clave;
    }
    
    private function enviarCorreo($correo, $clave) {
        $mail = new PHPMailer(true);
        $mail->CharSet = 'UTF-8';
        try {
            //Server settings
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'kbrenesrubi05@gmail.com';                     // SMTP username
            $mail->Password   = 'kev945405';                               // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port       = 25;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('kbrenesrubi05@gmail.com', 'Distribuidora Gonzalez');
            $mail->addAddress($correo, 'Cliente de Distribuidora Gonzalez');     // Add a recipient
            $mail->addReplyTo('kbrenesrubi05@gmail.com', 'Distribuidora Gonzalez');

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Distribuidora Gonzalez';
            $mail->Body    = 'Tú nueva contraseña de acceso es: <b>' . strval($clave) .'</b>';

            $mail->send();
        } catch (Exception $e) {
            return 0;
        }
        return 1;
    } 

    public function modificarClaveCliente($Correo_C){
        $nuevaClave = $this->generarClaveAleatoria($Correo_C);
        $clave = $this->encriptarClave($nuevaClave);
        try{
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare("CALL editar_Cliente_Clave(?,?);");
            $conexion->bind_param('ss',$Correo_C,$clave);
            $conexion->execute();

            if ($conexion->affected_rows >= 1) {
                return $this->enviarCorreo($Correo_C, $nuevaClave);
            }
            else {
                return 0;
            }

        }catch (Exception $ex){
            return $ex;

        }finally {
            mysqli_close($this->getConexion());
        }
        return $clave;
    }

    public function obtenerClienteLogin($Correo_C,$Clave) {
        try {
            $conexion = $this->getConexion()->stmt_init();
            $conexion->prepare( "call sodaonline.obtenerCliente(?);" );
            $conexion->bind_param("s", $Correo_C);
            $conexion->execute();
            $conexion->bind_result($Correo_C,$Nombre_C,$Password_C);

            $listaCliente = array();

            while ($conexion->fetch()) {
                $claveCliente =  $this->verificarClave($Clave, $Password_C); 
                $Cliente = (object) [
                    'Correo_C' => $Correo_C,
                    'Nombre_C' => $Nombre_C,
                    'Password_C' => $claveCliente
                ];

                array_push($listaCliente, $Cliente);
            }

            return json_encode($listaCliente);
        } catch (Exception $ex) {
            return $ex;
        }finally {
            mysqli_close($this->getConexion());
        }
    }
}

$cliente = new Cliente();

if ( (isset($_REQUEST['obtenerClientes'])) ) {
    echo $cliente->obtenerClientes();
}
if ( (isset($_REQUEST['obtenerClienteLogin'])) ) {
    echo $cliente->obtenerClienteLogin($_REQUEST['Correo_C'], $_REQUEST['Password_C']);
}
if ( (isset($_REQUEST['registrarClientes'])) ) {
    echo $cliente->registrarClientes( $_REQUEST['Correo_C'],$_REQUEST['Nombre_C'],$_REQUEST['Telefono_C'], $_REQUEST['Password_C']);
}
if ( (isset($_REQUEST['eliminarCliente'])) ) {
    echo $cliente->eliminarCliente( $_REQUEST['Correo_C']);
}
if ( (isset($_REQUEST['modificarCliente'])) ) {
    echo $cliente->modificarCliente( $_REQUEST['Correo_C'],$_REQUEST['Nombre_C'],$_REQUEST['Telefono_C'],$_REQUEST['Password_C']);
}

if ( (isset($_REQUEST['modificarClaveCliente'])) ) {
    echo $cliente->modificarClaveCliente( $_REQUEST['Correo_C']);
}


