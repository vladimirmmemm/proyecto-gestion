<!doctype html>
<html lang="es">
<head>

     <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" href="../lib/bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="../lib/alertifyjs/css/alertify.min.css">
    <link rel="stylesheet" href="../lib/sweetalert/sweetalert.css">
    <link rel="stylesheet" href="../css/login.css">

    <script src="../lib/jquery-3.2.1-dist/jquery-3.2.1.js"></script>
    <script src="../lib/bootstrap-4.3.1-dist/js/bootstrap.js"></script>
    <script src="../lib/alertifyjs/alertify.js"></script>
    <script src="../lib/sweetalert/sweetalert.js"></script>

    <script src="../script/general.js"></script>
    <script src="../script/loginCliente.js"></script>

    <title>LOGIN</title>
</head>
<body>


<div class="wrapper fadeInDown">
    <div id="formContent">
        <!-- Tabs Titles -->
        <div class="container mt-3">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="#login">LOGIN</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#registro">REGISTRO</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div id="login" class="container tab-pane active"><br>
                    <h3>Inicio de Sesión</h3>
                    <div>
                        <input type="text" id="user" class="fadeIn second" name="login" placeholder="Usuario">
                        <input type="password" id="password" class="fadeIn third" name="login" placeholder="Contraseña">
                          <span class="fa fa-eye" id="mostrar" onclick="mostrarContrasenia()" title="Mostrar contraseña" ></span>
                        <br>
                        <input type="submit" class="fadeIn fourth" value="Iniciar Sesión" onclick="login()">
                        <a href="javascript: recuperarClave();">Recuperar contraseña</a>
                    </div>
                </div>
                <div id="registro" class="container tab-pane fade"><br>
                    <h3>REGISTRO</h3>
                    <div>
                        <input type="text" id="correo" class="fadeIn second form-control" name="correo" placeholder="Correo Cliente"
                               onkeypress="return soloCorreo(event);" maxlength="100" required>
                        <input type="text" id="nombre" class="fadeIn third" name="login" placeholder="Nombre Cliente"
                               onkeypress="return soloLetras(event);" maxlength="45" required>
                        <input type="text" id="telefono" class="fadeIn second" name="telefono" placeholder="Teléfono Cliente"
                               onkeypress="return soloNumeros(event);" maxlength="8" required>
                        <input type="password" id="password1" class="fadeIn third form-control" name="login" placeholder="Contraseña Cliente"
                               onkeypress="return soloLetras(event);"maxlength="15" required>
                        <input type="password" id="password2" class="fadeIn third form-control" name="login" placeholder="Confirmar Contraseña"
                               onkeypress="return soloLetras(event);"maxlength="15" required>
                        <br>
                        <input type="button" class="fadeIn fourth" value="Registrarse" onclick="registrarse()">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>