<!doctype html>
<html lang="es">
<head>
    <link rel="stylesheet" href="../lib/bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="../lib/alertifyjs/css/alertify.min.css">
    <link rel="stylesheet" href="../css/login.css">

    <script src="../lib/jquery-3.2.1-dist/jquery-3.2.1.js"></script>
    <script src="../lib/bootstrap-4.3.1-dist/js/bootstrap.js"></script>
    <script src="../lib/alertifyjs/alertify.js"></script>

    <script src="../script/general.js"></script>
    <script src="../script/loginAdmin.js"></script>

    <title>LOGIN</title>
</head>
<body>
    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->
            <br>
            <h3>Inicio de Sesión</h3>
            <!-- Login Form -->
            <div>
                <input type="text" id="login" class="fadeIn second" name="login" placeholder="Usuario">
                <input type="password" id="password" class="fadeIn third" name="login" placeholder="Contraseña">
                <br>
                <input type="submit" class="fadeIn fourth" value="Iniciar Sesión" onclick="login()">
            </div>

        </div>
    </div>
</body>
</html>